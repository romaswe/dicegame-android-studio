package com.example.romaswe.dicegame;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;
import java.util.Random;

/*
    This class is filled with set and get and characteristics of a dice
*/

public class dice implements Comparable<dice>, Parcelable {

    int number;
    int dicevalue;
    boolean selected = false;

    public dice(int numb){
        number = numb;
    }

    public void roll(){
        if (!selected){
            Random r = new Random();
            dicevalue = r.nextInt(6)+1;
        }
    }

    public int getNumber() {
        return number;

    }

    public int getDicevalue() {
        return dicevalue;
    }

    public boolean getSelected(){
        return  selected;
    }

    public void setSelected(boolean sel) {
        selected = sel;
    }

    @Override
    public int compareTo(dice o){
        return (int)(this.dicevalue - o.dicevalue);
    }

    // get varibles
    public dice(Parcel in){
        number = in.readInt();
        dicevalue = in.readInt();
        selected = in.readInt() !=0;


    }

    @Override
    public int describeContents(){
        return 0;
    }


    // Save varibles
    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeInt(number);
        dest.writeInt(dicevalue);
        dest.writeInt(selected ? 1 : 0);


    }
    public static final Parcelable.Creator<dice> CREATOR
            = new Parcelable.Creator<dice>() {
        public dice createFromParcel(Parcel in) {
            return new dice(in);
        }
        public dice[] newArray(int size) {
            return new dice[size];
        }
    };


}
