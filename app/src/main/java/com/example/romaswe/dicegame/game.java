package com.example.romaswe.dicegame;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.res.TypedArrayUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.lang.ArrayUtils;


public class game implements Parcelable {


    // Declare varibles
    final private dice[] dices;
    private int rolls;
    private int currentround;
    private int rollsleft;
    private int score;
    private  boolean gameselect[];

    private int[] scores;
    private String[] gametype;

    private ArrayList<Integer> numbers;

    int value;

    // constructor for the game class to initialize variables
    public  game(){
        gametype = new String[12];
        scores = new int[12];
        score = 0;
        currentround = 1;
        rollsleft = 3;
        dices = new dice[6];
        gameselect = new boolean[12];

        for (int i = 0; i < gameselect.length; i++){
            gameselect[i] = false;
        }


        for (int i = 0; i < 6; i++){
            dices[i] = new dice(i);
            dices[i].dicevalue = 1;
        }

    }
    // Helper functions with geters and seters for the dices
    public dice[] getDices() {
        return dices;
    }

    public int getCurrentround() {
        return currentround;
    }

    public int getRolls() {
        return rolls;
    }

    public int getRollsleft() {
        return rollsleft;
    }

    public int getScore() {
        return score;
    }

    public int[] getScores() {
        return scores;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    public int getdicevalue(int i){
        return dices[i].dicevalue;
    }

    public void setCurrentround(int curr) {
        currentround = curr;
    }

    public void setRolls(int ro) {
        rolls = ro;
    }

    public void setRollsleft(int left) {
        rollsleft = left;
    }

    public void setScore(int s) {
        score = s;
    }

    public void setSelected(int i){
        dices[i].setSelected(!dices[i].getSelected());
    }

    public boolean getSelected(int i){
        return dices[i].getSelected();
    }

    public void setGameselect(int select) {
        gameselect[select] = true;
    }


    public boolean getGameselect(int i) {
        return gameselect[i];
    }

    public boolean[] getGameselectarr() {
        return gameselect;
    }

    public void setGameselectarr(boolean[] gameselect) {
        this.gameselect = gameselect;
    }

    public String[] getGametype() {
        return gametype;
    }

    public void setGametype(String[] gametype) {
        this.gametype = gametype;
    }

    // Function to roll each dice
    public void rollDice(){
        for (int i = 0; i < 6; i++){
            dices[i].roll();
        }
        if (getRollsleft() == 1){
            setRollsleft(3);
            for (int i = 0; i < 6; i++){
                dices[i].setSelected(false);
            }

        }else{
            setRollsleft(getRollsleft() - 1);

        }

    }

    // Restart game function that sets all variable to 0
    // This is mostly done becuse if you are in score screen and press back you can play a new game
    public void restartgame(){
        //Sets evrything to 0
        gametype = new String[12];
        scores = new int[12];
        score = 0;
        currentround = 1;
        rollsleft = 3;
        gameselect = new boolean[12];

        for (int i = 0; i < gameselect.length; i++){
            gameselect[i] = false;
        }


        for (int i = 0; i < 6; i++){
            dices[i] = new dice(i);
        }
    }
    // Endround is calld in the end of each round (after 3 rolls) and handles score and saves what type of scoretype was selected
    public void Endround(int option){
        int round = getCurrentround() - 1;
        //System.out.println("Game round: " + round);
        dice[] Adices = getDices();
        int s = 0;
        option = option + 3;
        // If low is choosen all dices with the value 3 and elss is sumed
        // else we calculate the score based on the target/option that is chosen
        if (option == 3){
            gametype[round] = "low";

            for(int i = 0; i < Adices.length; ++i ){
                //System.out.println("DICES : --------" +  Adices[i].getDicevalue());
                if (Adices[i].getDicevalue() <= 3){
                    s += Adices[i].getDicevalue();
                }
            }
            scores[round] = s;
            //score += s;
            s = 0;
        }else {
            gametype[round] = Integer.toString(option);
            Integer _numbers[]= new Integer[6];
            for(int i = 0; i < _numbers.length;++i){
                _numbers[i]= dices[i].getDicevalue();
            }
            numbers = new ArrayList<>(Arrays.asList(_numbers));
            Collections.sort(numbers,Collections.reverseOrder());
            //scores[round] = 0;
            calculateDiceScore(option);

        }
        //System.out.println("score : --------" +  scores[round]);
        setCurrentround(getCurrentround() + 1);
        score += scores[round];
        if (getCurrentround() == 11){
            restartgame();
        }
    }

    // score calculator for all the cases exept "low"
    private void calculateDiceScore(int target){
        int round = getCurrentround() - 1;
        boolean used_dices[] = new boolean[6];
        boolean potential_used_dices[];

        int sum;
        outerloop:
        for(int i = 0; i <numbers.size();++i){
            potential_used_dices = new boolean[6];
            sum = numbers.get(i);
            if (sum == target && !used_dices[i]){
                scores[round] += target;
                used_dices[i] = true;
            }

            for (int j = i+1; j < numbers.size(); ++j){
                if(sum + numbers.get(j) == target  && !used_dices[j] && !used_dices[i]){
                    used_dices[j] = true;
                    used_dices[j] = true;
                    for(int x = 0; x < potential_used_dices.length; ++x){
                        if(potential_used_dices[x]){
                            used_dices[x] = true;

                        }
                    }
                    scores[round] += target;
                    continue outerloop;
                }
                else if(sum + numbers.get(j) < target && !used_dices[j]){
                    potential_used_dices[j] = true;
                    sum += numbers.get(j);

                }

            }
        }
    }
    // get variables
    public game(Parcel in){
        gametype = in.createStringArray();
        scores = in.createIntArray();
        score = in.readInt();
        currentround = in.readInt();
        rollsleft = in.readInt();
        gameselect = in.createBooleanArray();
        dices = in.createTypedArray(dice.CREATOR);


    }

    @Override
    public int describeContents(){
        return 0;
    }


    // Save variables
    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeStringArray(gametype);
        dest.writeIntArray(scores);
        dest.writeInt(score);
        dest.writeInt(currentround);
        dest.writeInt(rollsleft);
        dest.writeBooleanArray(gameselect);
        dest.writeTypedArray(dices, 0);

    }


    public static final Parcelable.Creator<game> CREATOR
            = new Parcelable.Creator<game>() {
        public game createFromParcel(Parcel in) {
            return new game(in);
        }
        public game[] newArray(int size) {
            return new game[size];
        }
    };

}
