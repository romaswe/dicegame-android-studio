package com.example.romaswe.dicegame;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private game Game = new game();
    final private ImageButton[] dices = new ImageButton[6];
    private TextView roundcounter;
    private TextView gamecounter;
    private TextView scorecunter;
    int isselected;
    private Button button;
    private Button score;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = findViewById(R.id.rollDices);
        score = findViewById(R.id.score);

        roundcounter = findViewById(R.id.rollsleft);
        gamecounter =  findViewById(R.id.gameround);
        scorecunter =  findViewById(R.id.currentscore);

        // Checks if ther is a saved state
        if(savedInstanceState != null){
            Game = savedInstanceState.getParcelable("gamestate");
            boolean buttonstate = savedInstanceState.getBoolean("button");
            boolean scorestate = savedInstanceState.getBoolean("score");
            System.out.println(buttonstate);
            System.out.println(scorestate);

            button.setEnabled(buttonstate);
            score.setEnabled(scorestate);
        }else{
            Game = new game();
            score.setEnabled(false);
        }

        //spinner
        final Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gametype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // Get start number of rolls left
        int currentround = Game.getRollsleft();
        roundcounter.setText("Rollsleft: " + Integer.toString(currentround));

        // Get start number of gamround
        int gameround = Game.getCurrentround();
        gamecounter.setText("Gameround: " + Integer.toString(gameround));

        // Get score
        int currentscore = Game.getScore();
        scorecunter.setText("Totalscore: " + Integer.toString(currentscore));

        for (int i = 0; i < 6; i++){
            dices[i] = findViewById(R.id.imageButton+i);
            if (Game.getRollsleft() == 3){
                dices[i].setEnabled(false);
            }else
                dices[i].setEnabled(true);

            dices[i].setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                   int id = view.getId();
                   setselecterd(id);
                }
            });
        }

        // spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // set input as a varible to be used on scoreselect
               isselected = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        // roll
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // roll the dices
                roll();

                // disable button for each round when you shuld select scoring
                if (Game.getRollsleft() == 3){
                    button.setEnabled(false);
                    for (int i = 0; i < 6; i++){
                        dices[i].setEnabled(false);
                    }

                    Toast.makeText(MainActivity.this, R.string.selectgametype, Toast.LENGTH_SHORT).show();
                    score.setEnabled(true);
                }else{
                    for (int i = 0; i < 6; i++){
                        dices[i].setEnabled(true);
                    }
                }

            }

        });
        // Select scoretype
        score.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int[] scoreArray = new int[12];
                String[] gametype = new String[12];


                // check if the selected item alrady is used
                if (Game.getGameselect(isselected)){
                    Toast.makeText(MainActivity.this, R.string.alradyselekted, Toast.LENGTH_SHORT).show();

                }else{
                    Game.setGameselect(isselected);

                    button.setEnabled(true);
                    score.setEnabled(false);

                    scoreArray = Game.getScores();
                    gametype = Game.getGametype();

                    Game.Endround(isselected);

                    updateview();

                }
                // if its the last round we go to score screen
                if (Game.getCurrentround() - 1 == 0){
                    Intent myIntent = new Intent(MainActivity.this, scorescreen.class);
                    myIntent.putExtra("scoreArray", scoreArray);
                    myIntent.putExtra("gameselectarray", gametype);
                    MainActivity.this.startActivity(myIntent);
                }
            }

        });

        updateview();


    }

    // Svae instance
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("gamestate", Game);
        if (button.isEnabled()){
            outState.putBoolean("button", true);
        }else{
            outState.putBoolean("button", false);
        }
        if (score.isEnabled()){
            outState.putBoolean("score", true);
        }else{
            outState.putBoolean("score", false);

        }


    }

    // Roll function to roll dices
    private void roll(){
        Game.rollDice();
        updateview();
    }

    // update the rendering
    private void updateview(){
        // update images
        for (int i = 0; i < 6; i++){
            dices[i].setImageResource(getdiceimage(i));
        }

        // update rounds
        int currentround = Game.getRollsleft();
        roundcounter.setText("Rollsleft: " + Integer.toString(currentround));

        // update gameround
        int gameround = Game.getCurrentround();
        gamecounter.setText("Gameround: " + Integer.toString(gameround));

        // update currentscore
        int currentscore = Game.getScore();
        scorecunter.setText("Totalscore: " + Integer.toString(currentscore));
        int[] score = Game.getScores();
        //scorecunter.setText("Totalscore: " + Integer.toString(score[Game.getCurrentround()]));


    }

    // Dice images for white or grey dices
    private int getdiceimage(int i){
        int dicevalue = Game.getdicevalue(i);
        int imageadress = 0;
        if (!Game.getSelected(i)) {
            switch (dicevalue) {
                case 1:
                    imageadress = R.drawable.white1;
                    break;
                case 2:
                    imageadress = R.drawable.white2;
                    break;
                case 3:
                    imageadress = R.drawable.white3;
                    break;
                case 4:
                    imageadress = R.drawable.white4;
                    break;
                case 5:
                    imageadress = R.drawable.white5;
                    break;
                case 6:
                    imageadress = R.drawable.white6;
                    break;
            }
        }else{
                switch (dicevalue){
                    case 1:
                        imageadress = R.drawable.grey1;
                        break;
                    case 2:
                        imageadress = R.drawable.grey2;
                        break;
                    case 3:
                        imageadress = R.drawable.grey3;
                        break;
                    case 4:
                        imageadress = R.drawable.grey4;
                        break;
                    case 5:
                        imageadress = R.drawable.grey5;
                        break;
                    case 6:
                        imageadress = R.drawable.grey6;
                        break;
                }
            }


        return imageadress;

    }

    // Toogle if a dices is "saved" so it should not roll
    private void setselecterd(int i){
        switch (i){
            case R.id.imageButton:
                Game.setSelected(0);
                break;
            case R.id.imageButton2:
                Game.setSelected(1);
                break;
            case R.id.imageButton3:
                Game.setSelected(2);
                break;
            case R.id.imageButton4:
                Game.setSelected(3);
                break;
            case R.id.imageButton5:
                Game.setSelected(4);
                break;
            case R.id.imageButton6:
                Game.setSelected(5);
                break;
        }

        updateview();

    }

}
