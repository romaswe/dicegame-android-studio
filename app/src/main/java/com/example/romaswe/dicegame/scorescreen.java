package com.example.romaswe.dicegame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;

public class scorescreen extends AppCompatActivity {
    private TextView round1;
    private TextView round2;
    private TextView round3;
    private TextView round4;
    private TextView round5;
    private TextView round6;
    private TextView round7;
    private TextView round8;
    private TextView round9;
    private TextView round10;
    private TextView totalscoretext;

    int[] scorearray;
    String[] gametype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scorescreen);
        final Button gamebutton = findViewById(R.id.newgame);
        round1 = findViewById(R.id.round1);
        round2 = findViewById(R.id.round2);
        round3 = findViewById(R.id.round3);
        round4 = findViewById(R.id.round4);
        round5 = findViewById(R.id.round5);
        round6 = findViewById(R.id.round6);
        round7 = findViewById(R.id.round7);
        round8 = findViewById(R.id.round8);
        round9 = findViewById(R.id.round9);
        round10 = findViewById(R.id.round10);
        totalscoretext = findViewById(R.id.totalscore);



        // Values from mainactivity
        Bundle myBundle = getIntent().getExtras();
        scorearray = myBundle.getIntArray("scoreArray");
        gametype = myBundle.getStringArray("gameselectarray");

        // Checks if ther is a saved state
        if(savedInstanceState != null){
            scorearray = savedInstanceState.getIntArray("scorearr");
            gametype = savedInstanceState.getStringArray("gametype");
        }

        System.out.println(Arrays.toString(scorearray));

        // Scores from each round
        round1.setText("Round 1, Gameselect: " + gametype[0] + ", Score: " + Integer.toString(scorearray[0]));
        round2.setText("Round 2, Gameselect: " + gametype[1] + ", Score: " + Integer.toString(scorearray[1]));
        round3.setText("Round 3, Gameselect: " + gametype[2] + ", Score: " + Integer.toString(scorearray[2]));
        round4.setText("Round 4, Gameselect: " + gametype[3] + ", Score: " + Integer.toString(scorearray[3]));
        round5.setText("Round 5, Gameselect: " + gametype[4] + ", Score: " + Integer.toString(scorearray[4]));
        round6.setText("Round 6, Gameselect: " + gametype[5] + ", Score: " + Integer.toString(scorearray[5]));
        round7.setText("Round 7, Gameselect: " + gametype[6] + ", Score: " + Integer.toString(scorearray[6]));
        round8.setText("Round 8, Gameselect: " + gametype[7] + ", Score: " + Integer.toString(scorearray[7]));
        round9.setText("Round 9, Gameselect: " + gametype[8] + ", Score: " + Integer.toString(scorearray[8]));
        round10.setText("Round 10, Gameselect: " + gametype[9] + ", Score: " + Integer.toString(scorearray[9]));

        // Calculate sum from the array
        int sum=0;
        for(int i:scorearray)
            sum+=i;

        // totalscore
        totalscoretext.setText("Totalscore of game is: " + Integer.toString(sum));




        // New game button if you wwant to play one more time
        gamebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(scorescreen.this, Integer.toString(totalscoreofgame), Toast.LENGTH_SHORT).show();

                Intent myIntent = new Intent(scorescreen.this, MainActivity.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                scorescreen.this.startActivity(myIntent);


            }
        });
    }

    // Svae instance
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putIntArray("scorearr", scorearray);
        outState.putStringArray("gametype", gametype);

    }

}
